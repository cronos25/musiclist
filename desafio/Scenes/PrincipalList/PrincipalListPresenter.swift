//
//  PrincipalListPresenter.swift
//  desafio
//
//  Created by Searock Ruzario on 13-03-20.
//  Copyright (c) 2020 Alexis Sepulveda. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol PrincipalListPresentationLogic
{
  func presentSomething(response: PrincipalList.Something.Response)
}

class PrincipalListPresenter: PrincipalListPresentationLogic
{
  weak var viewController: PrincipalListDisplayLogic?
  
  // MARK: Do something
  
  func presentSomething(response: PrincipalList.Something.Response)
  {
    let viewModel = PrincipalList.Something.ViewModel()
    viewController?.displaySomething(viewModel: viewModel)
  }
}
