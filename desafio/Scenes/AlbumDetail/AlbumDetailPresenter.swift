//
//  AlbumDetailPresenter.swift
//  desafio
//
//  Created by Searock Ruzario on 13-03-20.
//  Copyright (c) 2020 Alexis Sepulveda. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol AlbumDetailPresentationLogic
{
  func presentSomething(response: AlbumDetail.Something.Response)
}

class AlbumDetailPresenter: AlbumDetailPresentationLogic
{
  weak var viewController: AlbumDetailDisplayLogic?
  
  // MARK: Do something
  
  func presentSomething(response: AlbumDetail.Something.Response)
  {
    let viewModel = AlbumDetail.Something.ViewModel()
    viewController?.displaySomething(viewModel: viewModel)
  }
}
