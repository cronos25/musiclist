//
//  SongDetailInteractor.swift
//  desafio
//
//  Created by Searock Ruzario on 13-03-20.
//  Copyright (c) 2020 Alexis Sepulveda. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol SongDetailBusinessLogic
{
  func doSomething(request: SongDetail.Something.Request)
}

protocol SongDetailDataStore
{
  //var name: String { get set }
}

class SongDetailInteractor: SongDetailBusinessLogic, SongDetailDataStore
{
  var presenter: SongDetailPresentationLogic?
  var worker: SongDetailWorker?
  //var name: String = ""
  
  // MARK: Do something
  
  func doSomething(request: SongDetail.Something.Request)
  {
    worker = SongDetailWorker()
    worker?.doSomeWork()
    
    let response = SongDetail.Something.Response()
    presenter?.presentSomething(response: response)
  }
}
